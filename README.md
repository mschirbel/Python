# Python para Data Science e Machine Learning

## Curso Udemy

https://www.udemy.com/python-para-data-science-e-machine-learning/

### Instalando o Ambiente

Para o curso utilizaremos o Jupyter Notebook
É um HTTP server para rodar os scripts de Python

Para instalar o Jupyter:

Tenha a versão correta do Python, no caso 3+
```
which python3
sudo apt-get install python3.5
```

Instale o pip3:

```
sudo apt-get install python3-pip
```


Dê um upgrade no pip3:

```
pip3 install --upgrade pip
```

Instale o Jupyter:

```
pip3 install jupyter
```

### Outro jeito de instalar o Jupyter

Caso acontece um erro na instalação é provavelmente devido as permissões que o SO da ao Python

Nesse caso, desinstale o pip3:

```
sudo python3 -m pip uninstall pip
```

Reinstale o pip3:
```
sudo apt install python3-pip --reinstall
```

Confira a versão do pip3:

```
pip3 -V
```

Agora instale o Jupyter Notebook via apt

```
sudo apt install jupyter-notebook
```

Rode o HTTP server:

```
jupyter-notebook
```

Para instalar qualquer biblioteca diretamente no Jupyter-Notebook:

```
https://jakevdp.github.io/blog/2017/12/05/installing-python-packages-from-jupyter/

import sys
!{sys.executable} -m pip install [package]
```
